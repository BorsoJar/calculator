package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity(){

    lateinit var calc : Calculating

       override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout)
        calc = Calculating(findViewById(R.id.textview_first))

        }

     fun onClick(v: View?) {

        when(v?.id){
            R.id.button0 -> calc.zero()
            R.id.button1 -> calc.one()
            R.id.button2 -> calc.two()
            R.id.button3 -> calc.three()
            R.id.button4 -> calc.four()
            R.id.button5 -> calc.five()
            R.id.button6 -> calc.six()
            R.id.button7 -> calc.seven()
            R.id.button8 -> calc.eight()
            R.id.button9 -> calc.nine()
            R.id.plus -> calc.plus()
            R.id.minus -> calc.min()
            R.id.molt -> calc.molt()
            R.id.log -> calc.logaritm()
            R.id.equal -> calc.equal()
            R.id.del -> calc.del()
            R.id.sen -> calc.sin()
            R.id.cos -> calc.cos()
        }

    }

}








